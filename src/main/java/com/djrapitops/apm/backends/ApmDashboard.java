package com.djrapitops.apm.backends;

import com.djrapitops.apm.backends.json.ApmJSONService;
import com.djrapitops.apm.backends.web.WebServer;
import com.djrapitops.apm.backends.web.login.PasswordStorage;
import com.djrapitops.apm.backends.web.login.TokenVerifier;
import com.djrapitops.apm.backends.web.login.YamlPasswordStorage;
import com.djrapitops.apm.exceptions.web.WebServerException;
import org.bukkit.configuration.InvalidConfigurationException;
import ru.tehkode.permissions.bukkit.APM;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Central class for initializing the Apm Dashboard.
 *
 * @author Rsl1122
 */
public class ApmDashboard {

	private final APM plugin;
	private Logger logger;

	private final WebServer webServer;

	private final ApmJSONService apmJSONService;
	private PasswordStorage passwordStorage;
	private TokenVerifier tokenVerifier;

	public ApmDashboard(APM plugin) {
		logger = plugin.getLogger();
		this.plugin = plugin;
		try {
			tokenVerifier = new TokenVerifier();
		} catch (UnsupportedEncodingException e) {
			logger.log(Level.SEVERE, "Failed to load token verifier, WebServer can not function: " + e.getMessage());
		}
		passwordStorage = new YamlPasswordStorage(plugin.getDataFolder());
		apmJSONService = new ApmJSONService(plugin);

		webServer = new WebServer(plugin, this);
	}

	public boolean isEnabled() {
		return webServer.isEnabled();
	}

	public void enable() throws WebServerException, IOException, InvalidConfigurationException {
		if (tokenVerifier == null) {
			return;
		}
		webServer.enable();

		// Create dashboard_users.yml file.
		if (webServer.isEnabled()) {
			logger.log(Level.INFO, "Loading dashboard users..");
			logger.log(Level.INFO, "Loaded " + passwordStorage.loadAndHash() + " users.");
		}
	}

	public void disable() {
		webServer.disable();
	}

	public WebServer getWebServer() {
		return webServer;
	}

	public PasswordStorage getPasswordStorage() {
		return passwordStorage;
	}

	public ApmJSONService getApmJSONService() {
		return apmJSONService;
	}

	public TokenVerifier getTokenVerifier() {
		return tokenVerifier;
	}

	public Logger getLogger() {
		return logger;
	}
}