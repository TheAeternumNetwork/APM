package com.djrapitops.apm.backends.web.pages;

import com.djrapitops.apm.backends.ApmDashboard;
import com.djrapitops.apm.backends.json.ApmJSONService;
import com.djrapitops.apm.backends.web.http.Request;
import com.djrapitops.apm.backends.web.http.Response;
import com.djrapitops.apm.backends.web.http.responses.JsonErrorResponse;
import com.djrapitops.apm.backends.web.pages.rest.*;

import java.util.List;

/**
 * PageHandler that will handle Rest API end points at /api.
 *
 * @author Rsl1122
 */
public class RestAPIPageHandler extends TreePageHandler {

	public RestAPIPageHandler(ApmDashboard apmDashboard) {

		ApmJSONService apmJSONService = apmDashboard.getApmJSONService();

		registerPage("users", new UserRestAPI(apmJSONService.getUserJSONService()));
		registerPage("groups", new GroupRestAPI(apmJSONService.getGroupJSONService()));
		registerPage("worlds", new WorldRestAPI(apmJSONService.getWorldJSONService()));
		registerPage("plugins", new PluginRestAPI(apmJSONService.getPluginJSONService()));
		registerPage("backups", new BackupRestAPI(apmJSONService.getBackupJSONService()));
		registerPage("login", new LoginRestAPI(apmDashboard.getTokenVerifier(), apmDashboard.getPasswordStorage()));
	}

	@Override
	public PageHandler getPageHandler(List<String> target) {
		PageHandler pageHandler = super.getPageHandler(target);
		return pageHandler != null ? pageHandler :
				new PageHandler() {
					@Override
					public Response getResponse(Request request, List<String> target) {
						return new JsonErrorResponse("API End-point not registered", 400);
					}
				};
	}
}