package com.djrapitops.apm.backends.web.pages;

import com.djrapitops.apm.backends.web.http.Request;
import com.djrapitops.apm.backends.web.http.Response;

import java.util.List;

/**
 * Interface for easier Response management.
 *
 * @author Rsl1122
 */
public interface PageHandler {

	Response getResponse(Request request, List<String> target);

}