package com.djrapitops.apm.backends.web.http.auth;

/**
 * Interface for HTTPS authentication methods.
 *
 * @author Rsl1122
 */
public interface Authentication {

	boolean isValid();

}