package ru.tehkode.permissions.bukkit.regexperms;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.atomic.AtomicReference;

import org.bukkit.entity.Player;
import org.bukkit.permissions.Permissible;
import org.bukkit.plugin.PluginManager;

import com.google.common.collect.Sets;

import ru.tehkode.permissions.bukkit.APM;
import ru.tehkode.utils.FieldReplacer;

/**
 * PermissibleMap for the permissions subscriptions data in Bukkit's {@link PluginManager} so we can put in our own data too.
 */
public class APMPermissionSubscriptionMap extends HashMap<String, Map<Permissible, Boolean>> {
	/**
	 *
	 */
	private static final long serialVersionUID = 6118884461598423736L;
	@SuppressWarnings("rawtypes")
	private static FieldReplacer<PluginManager, Map> INJECTOR;
	private static final AtomicReference<APMPermissionSubscriptionMap> INSTANCE = new AtomicReference<>();
	private final APM plugin;
	private final PluginManager manager;


	private APMPermissionSubscriptionMap(APM plugin, PluginManager manager, Map<String, Map<Permissible, Boolean>> backing) {
		super(backing);
		this.plugin = plugin;
		this.manager = manager;
	}

	/**
	 * Inject a APM permission subscription map into the provided plugin manager.
	 * This allows some APM functions to work with the plugin manager.
	 *
	 * @param manager The manager to inject into
	 */
	@SuppressWarnings({"unchecked", "rawtypes"})
	public static APMPermissionSubscriptionMap inject(APM plugin, PluginManager manager) {
		APMPermissionSubscriptionMap map = INSTANCE.get();
		if (map != null) {
			return map;
		}

		if (INJECTOR == null) {
			INJECTOR = new FieldReplacer<>(manager.getClass(), "permSubs", Map.class);
		}

		Map backing = INJECTOR.get(manager);
		if (backing instanceof APMPermissionSubscriptionMap) {
			return (APMPermissionSubscriptionMap) backing;
		}
		APMPermissionSubscriptionMap wrappedMap = new APMPermissionSubscriptionMap(plugin, manager, backing);
		if (INSTANCE.compareAndSet(null, wrappedMap)) {
			INJECTOR.set(manager, wrappedMap);
			return wrappedMap;
		} else {
			return INSTANCE.get();
		}
	}

	/**
	 * Uninject this APM map from its plugin manager
	 */
	public void uninject() {
		if (INSTANCE.compareAndSet(this, null)) {
			Map<String, Map<Permissible, Boolean>> unwrappedMap = new HashMap<>(this.size());
			for (Map.Entry<String, Map<Permissible, Boolean>> entry : this.entrySet()) {
				if (entry.getValue() instanceof APMSubscriptionValueMap) {
					unwrappedMap.put(entry.getKey(), ((APMSubscriptionValueMap) entry.getValue()).backing);
				}
			}
			INJECTOR.set(manager, unwrappedMap);
		}
	}

	@Override
	public Map<Permissible, Boolean> get(Object key) {
		if (key == null) {
			return null;
		}

		Map<Permissible, Boolean> result = super.get(key);
		if (result == null) {
			result = new APMSubscriptionValueMap((String) key, new WeakHashMap<Permissible, Boolean>());
			super.put((String) key, result);
		} else if (!(result instanceof APMSubscriptionValueMap)) {
			result = new APMSubscriptionValueMap((String) key, result);
			super.put((String) key, result);
		}
		return result;
	}

	@Override
	public Map<Permissible, Boolean> put(String key, Map<Permissible, Boolean> value) {
		if (!(value instanceof APMSubscriptionValueMap)) {
			value = new APMSubscriptionValueMap(key, value);
		}
		return super.put(key, value);
	}

	public class APMSubscriptionValueMap implements Map<Permissible, Boolean> {
		private final String permission;
		private final Map<Permissible, Boolean> backing;

		public APMSubscriptionValueMap(String permission, Map<Permissible, Boolean> backing) {
			this.permission = permission;
			this.backing = backing;
		}

		@Override
		public int size() {
			return backing.size();
		}

		@Override
		public boolean isEmpty() {
			return backing.isEmpty();
		}

		@Override
		public boolean containsKey(Object key) {
			return backing.containsKey(key) || (key instanceof Permissible && ((Permissible) key).isPermissionSet(permission));
		}

		@Override
		public boolean containsValue(Object value) {
			return backing.containsValue(value);
		}

		@Override
		public Boolean put(Permissible key, Boolean value) {
			return backing.put(key, value);
		}

		@Override
		public Boolean remove(Object key) {
			return backing.remove(key);
		}

		@Override
		public void putAll(Map<? extends Permissible, ? extends Boolean> m) {
			backing.putAll(m);
		}

		@Override
		public void clear() {
			backing.clear();
		}

		@Override
		public Boolean get(Object key) {
			if (key instanceof Permissible) {
				Permissible p = (Permissible) key;
				if (p.isPermissionSet(permission)) {
					return p.hasPermission(permission);
				}
			}
			return backing.get(key);
		}

		@Override
		public Set<Permissible> keySet() {
			Collection<? extends Player> players = plugin.getServer().getOnlinePlayers();
			Set<Permissible> apmMatches = new HashSet<Permissible>(players.size());
			for (Player player : players) {
				if (player.hasPermission(permission)) {
					apmMatches.add(player);
				}
			}
			return Sets.union(apmMatches, backing.keySet());
		}

		@Override
		public Collection<Boolean> values() {
			return backing.values();
		}

		@Override
		public Set<Entry<Permissible, Boolean>> entrySet() {
			return backing.entrySet();
		}
	}
}
