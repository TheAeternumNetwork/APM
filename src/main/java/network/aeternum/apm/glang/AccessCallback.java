package network.aeternum.apm.glang;

public interface AccessCallback<V>
{
	public V get();
}
